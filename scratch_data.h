// ---------------------------------------------------------------------
//
// Copyright (C) 2019 by the deal.II authors
//
// This file is part of the deal.II library.
//
// The deal.II library is free software; you can use it, redistribute
// it, and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// The full text of the license can be found in the file LICENSE.md at
// the top level directory of deal.II.
//
// ---------------------------------------------------------------------

#ifndef dealii_numerics_scratch_data_h
#define dealii_numerics_scratch_data_h

#include <deal.II/base/config.h>

#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/mapping_q1.h>

#include <boost/any.hpp>
#include <boost/core/demangle.hpp>

#include <algorithm>
#include <map>
#include <typeinfo>
#include <vector>

#ifdef DEAL_II_WITH_TRILINOS
#  include <Sacado.hpp>
#endif

DEAL_II_NAMESPACE_OPEN namespace WorkStream
{
  namespace
  {
    template <class T>
    std::string
    type_to_string(const T &t)
    {
      return boost::core::demangle(typeid(t).name());
    }
  } // namespace

  /**
   * Helper class to simplify the parallel assembly of linear and non-linear
   * problems, and the evaluation of finite element fields.
   *
   * This class is a drop in ScratchData to use with the WorkStream::run()
   * function and with the MeshWorker::mesh_loop function().
   *
   * The ScratchData class has two main goals:
   * - store FEValues, FEFaceValues, and FESubFaceValues objects, and provide a
   *   uniform interfaces to your algorithms when assemblying cell, face, or
   *   subface contributions into a global vector or matrix
   * - store arbitrary data types, that can be retreived by name, and provide a
   *   sensible interface for those use cases where the user may need temporary
   *   vectors of data at quadrature points
   */
  template <int dim, int spacedim = dim>
  class ScratchData
  {
  public:
    /**
     * Explicit constructor.
     */
    ScratchData(
      const Mapping<dim, spacedim> &      mapping,
      const FiniteElement<dim, spacedim> &fe,
      const Quadrature<dim> &             quadrature,
      const UpdateFlags &                 update_flags,
      const Quadrature<dim - 1> &face_quadrature   = Quadrature<dim - 1>(),
      const UpdateFlags &        face_update_flags = update_default)
      : mapping(&mapping)
      , fe(&fe)
      , cell_quad(quadrature)
      , face_quad(face_quadrature)
      , cell_flags(update_flags)
      , face_flags(face_update_flags)
      , local_dof_indices(fe.dofs_per_cell)
      , neighbor_dof_indices(fe.dofs_per_cell){};

    ScratchData(
      const FiniteElement<dim, spacedim> &fe,
      const Quadrature<dim> &             quadrature,
      const UpdateFlags &                 update_flags,
      const Quadrature<dim - 1> &face_quadrature   = Quadrature<dim - 1>(),
      const UpdateFlags &        face_update_flags = update_default)
      : ScratchData(StaticMappingQ1<dim, spacedim>::mapping,
                    fe,
                    quadrature,
                    update_flags,
                    face_quadrature,
                    face_update_flags){};

    /**
     * Deep copy constructor.
     */
    ScratchData(const ScratchData<dim, spacedim> &scratch)
      : mapping(scratch.mapping)
      , fe(scratch.fe)
      , cell_quad(scratch.cell_quad)
      , face_quad(scratch.face_quad)
      , cell_flags(scratch.cell_flags)
      , face_flags(scratch.face_flags)
      , local_dof_indices(scratch.local_dof_indices)
      , neighbor_dof_indices(scratch.neighbor_dof_indices)
      , cache(scratch.cache){};


    /**
     * Initialize the internal FEValues to use the given @p cell.
     */
    const FEValues<dim, spacedim> &
    reinit(const typename DoFHandler<dim, spacedim>::active_cell_iterator &cell)
    {
      if (!fe_values)
        fe_values = std::make_unique<FEValues<dim, spacedim>>(*mapping,
                                                              *fe,
                                                              cell_quad,
                                                              cell_flags);

      fe_values->reinit(cell);
      cell->get_dof_indices(local_dof_indices);
      add_ref<FEValuesBase<dim, spacedim>>(*fe_values, "FEValuesBase");
      return *fe_values;
    };


    /**
     * Initialize the internal FEFaceValues to use the given @p face_no on the given
     * @p cell.
     */
    const FEFaceValues<dim, spacedim> &
    reinit(const typename DoFHandler<dim, spacedim>::active_cell_iterator &cell,
           const unsigned int face_no)
    {
      if (!fe_face_values)
        fe_face_values = std::make_unique<FEFaceValues<dim, spacedim>>(
          *mapping, *fe, face_quad, face_flags);

      fe_face_values->reinit(cell, face_no);
      cell->get_dof_indices(local_dof_indices);
      add_ref<FEValuesBase<dim, spacedim>>(*fe_face_values, "FEValuesBase");
      return *fe_face_values;
    };


    /**
     * Initialize the internal FESubfaceValues to use the given @p subface_no, on @p face_no,
     * on the given @p cell.
     */
    const FEFaceValuesBase<dim, spacedim> &
    reinit(const typename DoFHandler<dim, spacedim>::active_cell_iterator &cell,
           const unsigned int face_no,
           const unsigned int subface_no)
    {
      if (subface_no != numbers::invalid_unsigned_int)
        {
          if (!fe_subface_values)
            fe_subface_values =
              std::make_unique<FESubfaceValues<dim, spacedim>>(*mapping,
                                                               *fe,
                                                               face_quad,
                                                               face_flags);
          fe_subface_values->reinit(cell, face_no, subface_no);
          cell->get_dof_indices(local_dof_indices);
          add_ref<FEValuesBase<dim, spacedim>>(*fe_subface_values,
                                               "FEValuesBase");
          return *fe_subface_values;
        }
      else
        return reinit(cell, face_no);
    }


    /**
     * Initialize the internal neighbour FEValues to use the given @p cell.
     */
    const FEValues<dim, spacedim> &
    reinit_neighbour(
      const typename DoFHandler<dim, spacedim>::active_cell_iterator &cell)
    {
      if (!neighbour_fe_values)
        neighbour_fe_values = std::make_unique<FEValues<dim, spacedim>>(
          *mapping, *fe, cell_quad, cell_flags);

      neighbour_fe_values->reinit(cell);
      cell->get_dof_indices(neighbor_dof_indices);
      add_ref<FEValuesBase<dim, spacedim>>(*neighbour_fe_values,
                                           "NeighborFEValuesBase");
      return *neighbour_fe_values;
    };


    /**
     * Initialize the internal FEFaceValues to use the given @p face_no on the given
     * @p cell.
     */
    const FEFaceValues<dim, spacedim> &
    reinit_neighbour(
      const typename DoFHandler<dim, spacedim>::active_cell_iterator &cell,
      const unsigned int                                              face_no)
    {
      if (!neighbour_fe_face_values)
        neighbour_fe_face_values =
          std::make_unique<FEFaceValues<dim, spacedim>>(*mapping,
                                                        *fe,
                                                        face_quad,
                                                        face_flags);
      neighbour_fe_face_values->reinit(cell, face_no);
      cell->get_dof_indices(neighbor_dof_indices);
      add_ref<FEValuesBase<dim, spacedim>>(*neighbour_fe_face_values,
                                           "NeighborFEValuesBase");
      return *neighbour_fe_face_values;
    };


    /**
     * Initialize the internal FESubfaceValues to use the given @p subface_no, on @p face_no,
     * on the given @p cell.
     */
    const FEFaceValuesBase<dim, spacedim> &
    reinit_neighbour(
      const typename DoFHandler<dim, spacedim>::active_cell_iterator &cell,
      const unsigned int                                              face_no,
      const unsigned int subface_no)
    {
      if (subface_no != numbers::invalid_unsigned_int)
        {
          if (!neighbour_fe_subface_values)
            neighbour_fe_subface_values =
              std::make_unique<FESubfaceValues<dim, spacedim>>(*mapping,
                                                               *fe,
                                                               face_quad,
                                                               face_flags);
          neighbour_fe_subface_values->reinit(cell, face_no, subface_no);
          cell->get_dof_indices(local_dof_indices);
          add_ref<FEValuesBase<dim, spacedim>>(*neighbour_fe_subface_values,
                                               "NeighborFEValuesBase");
          return *neighbour_fe_subface_values;
        }
      else
        return reinit_neighbour(cell, face_no);
    }


    /**
     * Get the currently initialized FEValues.
     *
     * This function will return the internal FEValues if the
     * reinit(cell) function was called last. If the reinit(cell, face_no)
     * function was called, then this function returns the internal
     * FEFaceValues.
     */
    const FEValuesBase<dim, spacedim> &
    get_current_fe_values()
    {
      Assert(have("FEValuesBase"),
             ExcMessage("You have to initialize the cache using one of the "
                        "reinit function first!"));
      FEValuesBase<dim, spacedim> &fev =
        get<FEValuesBase<dim, spacedim>>("FEValuesBase");
      return fev;
    }

    /**
     * Get the currently initialized neighbor FEValues.
     */
    const FEValuesBase<dim, spacedim> &
    get_current_neighbor_fe_values()
    {
      Assert(have("NeighborFEValuesBase"),
             ExcMessage("You have to initialize the cache using one of the "
                        "reinit function first!"));
      FEValuesBase<dim, spacedim> &fev =
        get<FEValuesBase<dim, spacedim>>("NeighborFEValuesBase");
      return fev;
    }

    /**
     * Store internally a copy of the given object. The copied object is
     * owned by this class, and is be accessible via the get() method.
     */
    template <typename type>
    void
    add_copy(const type &entry, const std::string &name)
    {
      cache[name] = entry;
    }

    /**
     * Add a reference to an already existing object. The object is not
     * owned by this class, and the user has to guarantee that the
     * referenced object lives longer than this class.
     */
    template <typename type>
    void
    add_ref(type &entry, const std::string &name)
    {
      type *ptr   = &entry;
      cache[name] = ptr;
    }

    /**
     * Return a reference to the object with given name. This function
     * throws an exception if either an object with the given name is not
     * stored in this class, or if the object with the given name is not of
     * compatible type.
     */
    template <typename type>
    type &
    get(const std::string &name)
    {
      Assert(cache.find(name) != cache.end(), ExcNameNotFound(name));

      type *p = NULL;

      if (cache[name].type() == typeid(type *))
        {
          p = boost::any_cast<type *>(cache[name]);
        }
      else if (cache[name].type() == typeid(type))
        {
          p = boost::any_cast<type>(&cache[name]);
        }
      else
        {
          Assert(false,
                 ExcTypeMismatch(typeid(type).name(),
                                 cache[name].type().name()));
        }

      return *p;
    };

    /**
     * Return a const reference to the object with the given name. This function
     * throws an exception if either the name does not exist or if the
     * object stored in the class with the given name cannot be converted to
     * the given type.
     */
    template <typename type>
    const type &
    get(const std::string &name) const
    {
      const auto it = cache.find(name);

      AssertThrow(cache.find(name) != cache.end(), ExcNameNotFound(name));

      if (it->second.type() == typeid(type *))
        {
          const type *p = boost::any_cast<type *>(it->second);
          return *p;
        }
      else if (it->second.type() == typeid(type))
        {
          const type *p = boost::any_cast<type>(&it->second);
          return *p;
        }
      else
        {
          Assert(false,
                 ExcTypeMismatch(typeid(type).name(),
                                 it->second.type().name()));
          const type *p = NULL;
          return *p;
        }
    };

    /**
     * Find out if we store an object with given name.
     *
     */
    inline bool
    have(const std::string &name) const
    {
      return cache.find(name) != cache.end();
    };

    /**
     *
     */
    template <typename Number = double>
    std::vector<Number> &
    get_current_independent_local_dofs(const std::string &prefix,
                                       Number             dummy = Number(0))
    {
      std::string dofs_name =
        prefix + "_independent_local_dofs_" + type_to_string(dummy);

      Assert(
        have(dofs_name),
        ExcMessage(
          "You did not call cache_local_solution_vector with the right types!"));

      return get<std::vector<Number>>(dofs_name);
    }


    /**
     * Return the quadrature points of the internal FEValues object.
     */
    const std::vector<Point<spacedim>> &
    get_quadrature_points()
    {
      return get_current_fe_values().get_quadrature_points();
    }
    const;


    /**
     * Return the JxW values of the internal FEValues object.
     */
    const std::vector<double> &
    get_JxW_values()
    {
      return get_current_fe_values().get_JxW_values();
    }
    const;

    /**
     * Return the JxW values of the neighbor FEValues object.
     */
    const std::vector<double> &
    get_neighbor_JxW_values()
    {
      return get_current_neighbor_fe_values().get_JxW_values();
    }
    const;

    /**
     * Return the last computed normal vectors.
     */
    const std::vector<Tensor<1, spacedim>> &
    get_normal_vectors()
    {
      return get_current_fe_values().get_normal_vectors();
    }

    /**
     * Return the last computed normal vectors on the neighbor.
     */
    const std::vector<Tensor<1, spacedim>> &
    get_neighbor_normal_vectors()
    {
      return get_current_neighbor_fe_values().get_normal_vectors();
    }

    /**
     * Print the content of the internal
     */
    template <class STREAM>
    void
    print_info(STREAM &os)
    {
      for (auto it : cache)
        {
          os << it->first << '\t' << '\t'
             << boost::core::demangle(it->second.type().name()) << std::endl;
        }
    }


    /**
     * Store internally the independent local dof values associated with the
     * internally initialized cell, face, or subface.
     *
     * Before you call this function, you have to make sure you have previously
     * called one of the reinit() functions.
     *
     * At every call of this function, a new vector of dof values is generated
     * and stored internally, unless a previous vector with the same name is
     * found. If this is the case, the content of that vector is overwritten.
     *
     * If you give a unique @p prefix, then for each cell you are guaranteed you
     * get a unique vector of independent dofs of the same type as the
     dummy
     * variable. DOFUtilities is used to initialize the variables, so if
     you
     * use a Sacado type, this will automatically initialize the
     derivatives
     * inside the vector.
     */
    template <typename VEC, typename Number = double>
    void
    cache_local_solution_vector(const std::string &prefix,
                                const VEC &        input_vector,
                                const Number       dummy = Number(0))
    {
#ifdef DEAL_II_WITH_TRILINOS
      typedef Sacado::Fad::DFad<double>  Sdouble;
      typedef Sacado::Fad::DFad<Sdouble> SSdouble;
#endif
      const unsigned int n_dofs =
        get_current_fe_values().get_fe().dofs_per_cell;

      std::string name =
        prefix + "_independent_local_dofs_" + type_to_string(dummy);

      if (!have(name))
        add_copy(std::vector<Number>(n_dofs), name);

      std::vector<Number> &independent_local_dofs =
        get<std::vector<Number>>(name);

      const unsigned int dofs_per_cell = local_dof_indices.size();

      for (unsigned int i = 0; i < dofs_per_cell; ++i)
        {
          if (typeid(Number) == typeid(double))
            {
              independent_local_dofs[i] = input_vector(local_dof_indices[i]);
            }
#ifdef DEAL_II_WITH_TRILINOS
          else if (typeid(Number) == typeid(Sdouble))
            {
              Sdouble ildv(dofs_per_cell,
                           i,
                           input_vector(local_dof_indices[i]));
              ((Sdouble &)independent_local_dofs[i]) = ildv;
            }
          else if (typeid(Number) == typeid(SSdouble))
            {
              SSdouble ildv(dofs_per_cell,
                            i,
                            global_vector(local_dof_indices[i]));
              ildv.val() =
                Sdouble(dofs_per_cell, i, input_vector(local_dof_indices[i]));
              ((SSdouble &)independent_local_dofs[i]) = ildv;
            }
#endif
          else
            {
              Assert(false, dealii::ExcNotImplemented());
            }
        }
    }



    //  /**
    //   * Return the values of the named cached solution vector.
    //   *
    //   * Before you call this function, you have to make sure you have
    //   previously
    //   * called the function cache_local_solution_vector with the same
    //   name you give
    //   * here and with the same dummy type you use here.
    //   */
    //  template <typename Number>
    //  const std::vector<std::vector<Number>> &
    //  get_values(const std::string &prefix, const Number dummy)
    //  {
    //    const std::vector<Number> &independent_local_dofs =
    //      get_current_independent_local_dofs(prefix, dummy);

    //    const FEValuesBase<dim, spacedim> &fev =
    //    get_current_fe_values();

    //    const unsigned int n_q_points   = fev.n_quadrature_points;
    //    const unsigned int n_components = fev.get_fe().n_components();

    //    std::string name = prefix + "_all_values_q" +
    //                       Utilities::int_to_string(n_q_points) + "_"
    //                       + type_to_string(dummy);

    //    if (!have(name))
    //      {
    //        add_copy(std::vector<std::vector<Number>>(
    //                   n_q_points, std::vector<Number>(n_components)),
    //                 name);
    //      }

    //    std::vector<std::vector<Number>> &ret =
    //      get<std::vector<std::vector<Number>>>(name);
    //    DOFUtilities::get_values(fev, independent_local_dofs, ret);
    //    return ret;
    //  }



    //  /**
    //   * Return the values of the named cached solution vector.
    //   *
    //   * Before you call this function, you have to make sure you have
    //   previously
    //   * called the function cache_local_solution_vector with the same @p prefix you give here
    //   * and with the same dummy type you use here.
    //   */
    //  template <typename Number>
    //  const std::vector<Number> &
    //  get_values(const std::string &                       prefix,
    //             const std::string & additional_prefix, const
    //             FEValuesExtractors::Scalar &variable, const Number dummy)
    //  {
    //    const std::vector<Number> &independent_local_dofs =
    //      get_current_independent_local_dofs(prefix, dummy);

    //    const FEValuesBase<dim, spacedim> &fev =
    //    get_current_fe_values();

    //    const unsigned int n_q_points = fev.n_quadrature_points;

    //    std::string name = prefix + "_" + additional_prefix +
    //    "_scalar_values_q"
    //    +
    //                       Utilities::int_to_string(n_q_points) + "_"
    //                       + type_to_string(dummy);

    //    // Now build the return type
    //    typedef typename std::vector<Number> RetType;


    //    if (!have(name))
    //      add_copy(RetType(n_q_points), name);

    //    RetType &ret = get<RetType>(name);
    //    DOFUtilities::get_values(fev, independent_local_dofs, variable,
    //    ret); return ret;
    //  }



    //  /**
    //   * Return the values of the named cached solution vector.
    //   *
    //   * Before you call this function, you have to make sure you have
    //   previously
    //   * called the function cache_local_solution_vector with the same @p prefix you give here
    //   * and with the same dummy type you use here.
    //   */
    //  template <typename Number>
    //  const std::vector<Tensor<1, spacedim, Number>> &
    //  get_values(const std::string &                       prefix,
    //             const std::string & additional_prefix, const
    //             FEValuesExtractors::Vector &vector_variable, const Number
    //             dummy)
    //  {
    //    const std::vector<Number> &independent_local_dofs =
    //      get_current_independent_local_dofs(prefix, dummy);

    //    const FEValuesBase<dim, spacedim> &fev =
    //    get_current_fe_values();

    //    const unsigned int n_q_points = fev.n_quadrature_points;

    //    std::string name = prefix + "_" + additional_prefix +
    //    "_vector_values_q"
    //    +
    //                       Utilities::int_to_string(n_q_points) + "_"
    //                       + type_to_string(dummy);


    //    // Now build the return type
    //    typedef typename std::vector<Tensor<1, spacedim, Number>>
    //    RetType;

    //    if (!have(name))
    //      add_copy(RetType(n_q_points), name);

    //    RetType &ret = get<RetType>(name);
    //    DOFUtilities::get_values(fev, independent_local_dofs,
    //    vector_variable, ret); return ret;
    //  }



    //  /**
    //   * Return the divergence of the named cached solution vector.
    //   *
    //   * Before you call this function, you have to make sure you have
    //   previously
    //   * called the function cache_local_solution_vector with the same @p prefix you give here
    //   * and with the same dummy type you use here.
    //   */
    //  template <typename Number>
    //  const std::vector<Number> &
    //  get_divergences(const std::string &                       prefix,
    //                  const std::string & additional_prefix, const
    //                  FEValuesExtractors::Vector &vector_variable,
    //                  const Number                              dummy)
    //  {
    //    const std::vector<Number> &independent_local_dofs =
    //      get_current_independent_local_dofs(prefix, dummy);

    //    const FEValuesBase<dim, spacedim> &fev =
    //    get_current_fe_values();

    //    const unsigned int n_q_points = fev.n_quadrature_points;

    //    std::string name = prefix + "_" + additional_prefix +
    //    "_div_values_q"
    //    +
    //                       Utilities::int_to_string(n_q_points) + "_"
    //                       + type_to_string(dummy);


    //    // Now build the return type
    //    typedef typename std::vector<Number> RetType;

    //    if (!have(name))
    //      add_copy(RetType(n_q_points), name);

    //    RetType &ret = get<RetType>(name);
    //    DOFUtilities::get_divergences(fev,
    //                                  independent_local_dofs,
    //                                  vector_variable,
    //                                  ret);
    //    return ret;
    //  }



    //  /**
    //   * Return the gradient of the named cached solution vector.
    //   *
    //   * Before you call this function, you have to make sure you have
    //   previously
    //   * called the function cache_local_solution_vector with the same @p prefix you give here
    //   * and with the same dummy type you use here.
    //   */
    //  template <typename Number>
    //  const std::vector<Tensor<1, spacedim, Number>> &
    //  get_gradients(const std::string &                       prefix,
    //                const std::string & additional_prefix, const
    //                FEValuesExtractors::Scalar &variable, const Number
    //                dummy)
    //  {
    //    const std::vector<Number> &independent_local_dofs =
    //      get_current_independent_local_dofs(prefix, dummy);

    //    const FEValuesBase<dim, spacedim> &fev =
    //    get_current_fe_values();

    //    const unsigned int n_q_points = fev.n_quadrature_points;

    //    std::string name = prefix + "_" + additional_prefix +
    //    "_grad_values_q"
    //    +
    //                       Utilities::int_to_string(n_q_points) + "_"
    //                       + type_to_string(dummy);

    //    // Now build the return type
    //    typedef typename std::vector<Tensor<1, spacedim, Number>>
    //    RetType;

    //    if (!have(name))
    //      add_copy(RetType(n_q_points), name);

    //    RetType &ret = get<RetType>(name);
    //    DOFUtilities::get_gradients(fev, independent_local_dofs, variable,
    //    ret); return ret;
    //  }



    //  /**
    //   * Return the gradient of the named cached solution vector.
    //   *
    //   * Before you call this function, you have to make sure you have
    //   previously
    //   * called the function cache_local_solution_vector with the same @p prefix you give here
    //   * and with the same dummy type you use here.
    //   */
    //  template <typename Number>
    //  const std::vector<Tensor<2, spacedim, Number>> &
    //  get_gradients(const std::string &                       prefix,
    //                const std::string & additional_prefix, const
    //                FEValuesExtractors::Vector &variable, const Number
    //                dummy)
    //  {
    //    const std::vector<Number> &independent_local_dofs =
    //      get_current_independent_local_dofs(prefix, dummy);

    //    const FEValuesBase<dim, spacedim> &fev =
    //    get_current_fe_values();

    //    const unsigned int n_q_points = fev.n_quadrature_points;

    //    std::string name = prefix + "_" + additional_prefix +
    //    "_grad2_values_q"
    //    +
    //                       Utilities::int_to_string(n_q_points) + "_"
    //                       + type_to_string(dummy);

    //    // Now build the return type
    //    typedef typename std::vector<Tensor<2, spacedim, Number>>
    //    RetType;

    //    if (!have(name))
    //      add_copy(RetType(n_q_points), name);

    //    RetType &ret = get<RetType>(name);
    //    DOFUtilities::get_gradients(fev, independent_local_dofs, variable,
    //    ret); return ret;
    //  }

    //  /**
    //   * Return the curl of the named cached solution vector.
    //   *
    //   * Before you call this function, you have to make sure you have
    //   previously
    //   * called the function cache_local_solution_vector with the same @p prefix you give here
    //   * and with the same dummy type you use here.
    //   */
    //  template <typename Number>
    //  const std::vector<Tensor<1, (spacedim > 2 ? spacedim : 1),
    //  Number>> & get_curls(const std::string & prefix,
    //            const std::string & additional_prefix, const
    //            FEValuesExtractors::Vector &variable, const Number dummy)
    //  {
    //    const std::vector<Number> &independent_local_dofs =
    //      get_current_independent_local_dofs(prefix, dummy);

    //    const FEValuesBase<dim, spacedim> &fev =
    //    get_current_fe_values();

    //    const unsigned int n_q_points = fev.n_quadrature_points;

    //    std::string name = prefix + "_" + additional_prefix +
    //    "_curl_values_q"
    //    +
    //                       Utilities::int_to_string(n_q_points) + "_"
    //                       + type_to_string(dummy);

    //    // Now build the return type
    //    typedef typename std::vector<
    //      Tensor<1, (spacedim > 2 ? spacedim : 1), Number>>
    //      RetType;

    //    if (!have(name))
    //      add_copy(RetType(n_q_points), name);

    //    RetType &ret = get<RetType>(name);
    //    DOFUtilities::get_curls(fev, independent_local_dofs, variable,
    //    ret); return ret;
    //  }

    //  /**
    //   * Return the laplacian of the named cached solution vector.
    //   *
    //   * Before you call this function, you have to make sure you have
    //   previously
    //   * called the function cache_local_solution_vector with the same @p prefix you give here
    //   * and with the same dummy type you use here.
    //   */
    //  template <typename Number>
    //  const std::vector<Number> &
    //  get_laplacians(const std::string &                       prefix,
    //                 const std::string & additional_prefix, const
    //                 FEValuesExtractors::Scalar &variable, const
    //                 Number dummy)
    //  {
    //    const std::vector<Number> &independent_local_dofs =
    //      get_current_independent_local_dofs(prefix, dummy);

    //    const FEValuesBase<dim, spacedim> &fev =
    //    get_current_fe_values();

    //    const unsigned int n_q_points = fev.n_quadrature_points;

    //    std::string name =
    //      prefix + "_" + additional_prefix + "_laplacian_values_q" +
    //      Utilities::int_to_string(n_q_points) + "_" +
    //      type_to_string(dummy);

    //    // Now build the return type
    //    typedef typename std::vector<Number> RetType;

    //    if (!have(name))
    //      add_copy(RetType(n_q_points), name);

    //    RetType &ret = get<RetType>(name);
    //    DOFUtilities::get_laplacians(fev, independent_local_dofs,
    //    variable, ret); return ret;
    //  }


    //  /**
    //   * Return the laplacian of the named cached solution vector. Vector
    //   value
    //   * case.
    //   *
    //   * Before you call this function, you have to make sure you have
    //   previously
    //   * called the function cache_local_solution_vector with the same @p prefix you give here
    //   * and with the same dummy type you use here.
    //   */
    //  template <typename Number>
    //  const std::vector<Tensor<1, spacedim, Number>> &
    //  get_laplacians(const std::string &                       prefix,
    //                 const std::string & additional_prefix, const
    //                 FEValuesExtractors::Vector &variable, const
    //                 Number dummy)
    //  {
    //    const std::vector<Number> &independent_local_dofs =
    //      get_current_independent_local_dofs(prefix, dummy);

    //    const FEValuesBase<dim, spacedim> &fev =
    //    get_current_fe_values();

    //    const unsigned int n_q_points = fev.n_quadrature_points;

    //    std::string name =
    //      prefix + "_" + additional_prefix + "_laplacian2_values_q" +
    //      Utilities::int_to_string(n_q_points) + "_" +
    //      type_to_string(dummy);

    //    // Now build the return type
    //    typedef typename std::vector<Tensor<1, spacedim, Number>>
    //    RetType;

    //    if (!have(name))
    //      add_copy(RetType(n_q_points), name);

    //    RetType &ret = get<RetType>(name);
    //    DOFUtilities::get_laplacians(fev, independent_local_dofs,
    //    variable, ret); return ret;
    //  }

    //  /**
    //   * Return the deformation gradient of the named cached solution
    //   vector.
    //   *
    //   * Before you call this function, you have to make sure you have
    //   previously
    //   * called the function cache_local_solution_vector with the same @p prefix you give here
    //   * and with the same dummy type you use here.
    //   */
    //  template <typename Number>
    //  const std::vector<Tensor<2, spacedim, Number>> &
    //  get_deformation_gradients(const std::string &prefix,
    //                            const std::string &additional_prefix,
    //                            const FEValuesExtractors::Vector
    //                            &variable, const Number dummy)
    //  {
    //    const std::vector<Number> &independent_local_dofs =
    //      get_current_independent_local_dofs(prefix, dummy);

    //    const FEValuesBase<dim, spacedim> &fev =
    //    get_current_fe_values();

    //    const unsigned int n_q_points = fev.n_quadrature_points;

    //    std::string name = prefix + "_" + additional_prefix +
    //    "_F_values_q" +
    //                       Utilities::int_to_string(n_q_points) + "_"
    //                       + type_to_string(dummy);

    //    // Now build the return type
    //    typedef typename std::vector<Tensor<2, spacedim, Number>>
    //    RetType;

    //    if (!have(name))
    //      add_copy(RetType(n_q_points), name);

    //    RetType &ret = get<RetType>(name);
    //    DOFUtilities::get_deformation_gradients(fev,
    //                                            independent_local_dofs,
    //                                            variable,
    //                                            ret);
    //    return ret;
    //  }



    //  /**
    //   * Return the symmetric gradient of the named cached solution
    //   vector.
    //   *
    //   * Before you call this function, you have to make sure you have
    //   previously
    //   * called the function cache_local_solution_vector with the same @p prefix you give here
    //   * and with the same dummy type you use here.
    //   */
    //  template <typename Number>
    //  const std::vector<Tensor<2, spacedim, Number>> &
    //  get_symmetric_gradients(const std::string &prefix,
    //                          const std::string &additional_prefix,
    //                          const FEValuesExtractors::Vector
    //                          &variable, const Number dummy)
    //  {
    //    const std::vector<Number> &independent_local_dofs =
    //      get_current_independent_local_dofs(prefix, dummy);

    //    const FEValuesBase<dim, spacedim> &fev =
    //    get_current_fe_values();

    //    const unsigned int n_q_points = fev.n_quadrature_points;

    //    std::string name = prefix + "_" + additional_prefix +
    //    "_sym_grad_values_q" +
    //                       Utilities::int_to_string(n_q_points) + "_"
    //                       + type_to_string(dummy);

    //    // Now build the return type
    //    typedef typename std::vector<Tensor<2, spacedim, Number>>
    //    RetType;

    //    if (!have(name))
    //      add_copy(RetType(n_q_points), name);

    //    RetType &ret = get<RetType>(name);
    //    DOFUtilities::get_symmetric_gradients(fev,
    //                                          independent_local_dofs,
    //                                          variable,
    //                                          ret);
    //    return ret;
    //  }

    //  /**
    //   * Return the hessian of the named cached solution vector.
    //   *
    //   * Before you call this function, you have to make sure you have
    //   previously
    //   * called the function cache_local_solution_vector with the same @p prefix you give here
    //   * and with the same dummy type you use here.
    //   */
    //  template <typename Number>
    //  const std::vector<Tensor<2, spacedim, Number>> &
    //  get_hessians(const std::string &                       prefix,
    //               const std::string & additional_prefix, const
    //               FEValuesExtractors::Scalar &variable, const Number
    //               dummy)
    //  {
    //    const std::vector<Number> &independent_local_dofs =
    //      get_current_independent_local_dofs(prefix, dummy);

    //    const FEValuesBase<dim, spacedim> &fev =
    //    get_current_fe_values();

    //    const unsigned int n_q_points = fev.n_quadrature_points;

    //    std::string name = prefix + "_" + additional_prefix +
    //    "_hessians_values_q" +
    //                       Utilities::int_to_string(n_q_points) + "_"
    //                       + type_to_string(dummy);

    //    // Now build the return type
    //    typedef typename std::vector<Tensor<2, spacedim, Number>>
    //    RetType;

    //    if (!have(name))
    //      add_copy(RetType(n_q_points), name);

    //    RetType &ret = get<RetType>(name);
    //    DOFUtilities::get_hessians(fev, independent_local_dofs, variable,
    //    ret); return ret;
    //  }

    //  /**
    //   * Return the hessian of the named cached solution vector. Vector
    //   value case.
    //   *
    //   * Before you call this function, you have to make sure you have
    //   previously
    //   * called the function cache_local_solution_vector with the same @p prefix you give here
    //   * and with the same dummy type you use here.
    //   */
    //  template <typename Number>
    //  const std::vector<Tensor<3, spacedim, Number>> &
    //  get_hessians(const std::string &                       prefix,
    //               const std::string & additional_prefix, const
    //               FEValuesExtractors::Vector &variable, const Number
    //               dummy)
    //  {
    //    const std::vector<Number> &independent_local_dofs =
    //      get_current_independent_local_dofs(prefix, dummy);

    //    const FEValuesBase<dim, spacedim> &fev =
    //    get_current_fe_values();

    //    const unsigned int n_q_points = fev.n_quadrature_points;

    //    std::string name =
    //      prefix + "_" + additional_prefix + "_hessians2_values_q" +
    //      Utilities::int_to_string(n_q_points) + "_" +
    //      type_to_string(dummy);

    //    // Now build the return type
    //    typedef typename std::vector<Tensor<3, spacedim, Number>>
    //    RetType;

    //    if (!have(name))
    //      add_copy(RetType(n_q_points), name);

    //    RetType &ret = get<RetType>(name);
    //    DOFUtilities::get_hessians(fev, independent_local_dofs, variable,
    //    ret); return ret;
    //  }

    /// An entry with this name does not exist in the boost::any map.
    DeclException1(ExcNameNotFound,
                   std::string,
                   << "No entry with the name " << arg1 << " exists.");

    /// The requested type and the stored type are different
    DeclException2(ExcTypeMismatch,
                   const char *,
                   const char *,
                   << "The requested type " << arg1 << " and the stored type "
                   << arg2 << " must coincide");

    const std::vector<types::global_dof_index> &
    get_local_dof_indices() const
    {
      return local_dof_indices;
    };

    const std::vector<types::global_dof_index> &
    get_neighbor_dof_indices() const
    {
      return neighbor_dof_indices;
    };

  private:
    SmartPointer<const FiniteElement<dim, spacedim>> fe;
    SmartPointer<const Mapping<dim, spacedim>>       mapping;

    Quadrature<dim>     cell_quad;
    Quadrature<dim - 1> face_quad;

    UpdateFlags cell_flags;
    UpdateFlags face_flags;

    std::unique_ptr<FEValues<dim, spacedim>>        fe_values;
    std::unique_ptr<FEFaceValues<dim, spacedim>>    fe_face_values;
    std::unique_ptr<FESubfaceValues<dim, spacedim>> fe_subface_values;

    std::unique_ptr<FEValues<dim, spacedim>>        neighbour_fe_values;
    std::unique_ptr<FEFaceValues<dim, spacedim>>    neighbour_fe_face_values;
    std::unique_ptr<FESubfaceValues<dim, spacedim>> neighbour_fe_subface_values;

    std::vector<types::global_dof_index> local_dof_indices;
    std::vector<types::global_dof_index> neighbor_dof_indices;

    std::map<std::string, boost::any> cache;
  };

} // namespace WorkStream
DEAL_II_NAMESPACE_CLOSE

#endif
