// ---------------------------------------------------------------------
//
// Copyright (C) 2019 by the deal.II authors
//
// This file is part of the deal.II library.
//
// The deal.II library is free software; you can use it, redistribute
// it, and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// The full text of the license can be found in the file LICENSE.md at
// the top level directory of deal.II.
//
// ---------------------------------------------------------------------

#ifndef dealii_numerics_copy_data_h
#define dealii_numerics_copy_data_h

#include <deal.II/base/config.h>

#include <deal.II/base/types.h>

#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/vector.h>

#include <vector>

DEAL_II_NAMESPACE_OPEN

namespace WorkStream
{
  /**
   * Helper copy data class.
   */
  template <int n_matrices    = 1,
            int n_vectors     = n_matrices,
            int n_dof_indices = n_matrices>
  struct CopyData
  {
    CopyData(const unsigned int size)
    {
      for (auto &m : matrices)
        m.reinit({size, size});
      for (auto &v : vectors)
        v.reinit(size);
      for (auto &d : local_dof_indices)
        d.resize(size);
    };

    CopyData(
      const std::array<std::array<unsigned int, 2>, n_matrices> &matrix_sizes,
      const std::array<unsigned int, n_vectors> &                vector_sizes,
      const std::array<unsigned int, n_dof_indices> &dof_indices_sizes)
    {
      unsigned int i = 0;
      for (auto &m : matrices)
        m.reinit(matrix_sizes[i]);
      i = 0;
      for (auto &v : vectors)
        v.reinit(vector_sizes[i]);
      i = 0;
      for (auto &d : local_dof_indices)
        d.resize(dof_indices_sizes[i]);
    };


    CopyData(const CopyData<n_matrices, n_vectors, n_dof_indices> &other) =
      default;

    void
    operator=(const double &number)
    {
      for (auto &m : matrices)
        m = number;
      for (auto &v : vectors)
        v = number;
    }

    std::array<FullMatrix<double>, n_matrices> matrices;
    std::array<Vector<double>, n_vectors>      vectors;
    std::array<std::vector<types::global_dof_index>, n_dof_indices>
      local_dof_indices;
  };
} // namespace WorkStream

DEAL_II_NAMESPACE_CLOSE

#endif
